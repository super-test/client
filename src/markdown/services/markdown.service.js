(function (angular, markdownit) {
    'use strict';

    angular.module('ll.markdown').provider('$markdown', [function() {
        return {
            opts: {},
            plugins: [],
            config: function(options) {
                this.opts = options;
            },
            registerPlugin: function(plugin) {
                this.plugins.push(plugin);
            },
            $get: function() {
                var md = markdownit(this.opts);
                md.renderer.rules.link_open = function (tokens, idx) {
                    var title = tokens[idx].title ? (' title="' + md.utils.escapeHtml(md.utils.replaceEntities(tokens[idx].title)) + '"') : '';
                    var target = tokens[idx].target ? (' target="' + md.utils.escapeHtml(tokens[idx].target) + '"') : ' target="_blank"';
                    return '<a href="' + md.utils.escapeHtml(tokens[idx].href) + '"' + title + target + '>';
                };

                if (this.plugins.length !== 0) {
                    for (var i = 0; i < this.plugins.length; i++) {
                        md.use(this.plugins[i]);
                    }
                }
                 
                return md; 
            }
        }

    }]);
})(window.angular, window.markdownit);
