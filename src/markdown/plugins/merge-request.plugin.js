function merge_request_plugin(md) {
    function mrrule(state, silent) {
        var start = state.pos,
            max   = state.posMax;

        if (state.src.charCodeAt(start) !== 0x21) {
            return false;
        }
        if (silent) {
            return false;
        }

        var mr = state.src.slice(start + 1, max);

        state.push({ type: 'link_open', href: state.env.host_url + '/merge_requests/' + mr, title: mr, target: "_blank", level: state.level++ });
        state.push({ type: 'text', content: "!" + mr, level: state.level });
        state.push({ type: 'link_close', level: --state.level });

        state.pos = max;

        return true;
    }

    md.inline.ruler.push('mrrule', mrrule);
}

