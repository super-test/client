(function(angular) {
    'use strict';
    angular.module('gitlabKBApp.board').factory('MilestoneService', 
        [
            '$http',
            '$q',
            function ($http, $q) {
                return {
                    milestones: {},
                    list: function (projectId) {
                        return $q.when(this.milestones[projectId] === undefined ? $http.get('api/milestones', {params: {project_id: projectId}}).then(function (result) {
                                this.milestones[projectId] = result.data.data;
                                return this.milestones[projectId];
                            }.bind(this)) : this.milestones[projectId]
                        );
                    }
                };
            }
        ]
    );
})(window.angular);


