(function(angular) {
    'use strict';
    angular.module('gitlabKBApp.board').factory('BoardService', 
        [
            '$http',
            '$q',
            '$sce',
            'LabelService',
            'WebsocketService',
            function ($http, $q, $sce, LabelService, WebsocketService) {
                var service = {
                    boardsList: {},
                    boards: {},
                    get: function (id) {

                        return $q.when(_.isEmpty(this.boards[id]) ? $q.all([
                            LabelService.list(id),
                            $http.get('/api/cards', {params: {project_id: id}})
                        ]).then(function(results) {
                            var labels = results[0];
                            var issues  = results[1].data.data;
                            var pattern = /KB\[stage\]\[\d\]\[(.*)\]/;

                            var stages = _.sortBy(_.filter(labels, function(label) {
                                return pattern.test(label.name);
                            }), "name");
                            if (_.isEmpty(stages)) {
                                return {};
                            }

                            var board = {
                                viewLabels: _.indexBy(_.difference(labels, stages), 'name'),
                                stages: [],
                                labels: _.pluck(stages, 'name')
                            };

                            issues = _.map(issues, function(issue, key) {
                                issue.viewLabels = [];

                                if (!_.isEmpty(issue.labels)) {
                                    var labels = issue.labels;
                                    for (var i = 0; i < labels.length; i++) {
                                        var label = board.viewLabels[labels[i]];
                                        if (label !== undefined) {
                                            issue.viewLabels.push(label);
                                        }
                                    }
                                }

                                return issue;
                            });

                            for (var i in stages) {
                                board.stages.push({
                                    label: stages[i].name,
                                    cards: [],
                                    title: stages[i].name.match(pattern)[1]
                                });
                            }

                            var cards = _.groupBy(issues, function(issue) {
                                var stage = _.intersection(board.labels, issue.labels);
                                if (stage.length === 0) {
                                    return board.labels[0];
                                } else {
                                    return stage[0];
                                }
                            });

                            for (var stage in board.stages) {
                                board.stages[stage].cards = cards[board.stages[stage].label];
                            }

                            this.boards[id] = board;

                            return this.boards[id];
                        }.bind(this)) : this.boards[id]);

                    },
                    getCard: function (boardId, cardId) {
                        return this.get(boardId).then(function (result) {
                            for (var stageName in result.stages) {
                                var stage = result.stages[stageName];
                                var card = _.find(stage.cards, function(card){return card.id == cardId});
                                if (card !== undefined) {
                                    return card;
                                }
                            }
                        });
                    },
                    createCard: function (data) {
                        return $http.post('/api/card', data).then(function (newCard) {
                        });
                    },
                    updateCard: function(card) {
                        return $http.put('/api/card', {
                            issue_id: card.id,
                            project_id: card.project_id,
                            assignee_id: card.assignee_id,
                            milestone_id: card.milestone_id,
                            title: card.title,
                            labels: card.labels.join(', '),
                            todo: card.todo,
                            description: card.description
                        }).then(function(result) {
                        });
                    },
                    removeCard: function (project_id, card) {
                        var _this = this;
                        return $http.delete('/api/card', {
                            data: {
                                project_id: project_id,
                                issue_id: card.id,
                                closed: 1
                            },
                            headers: {'Content-Type': 'application/json'}
                        }).then(function (result) {
                            return _this.removeCardFromBoard(card);
                        });
                    },
                    getBoards: function () {
                        var _this = this;
                        if (_.isEmpty(_this.boardsList)) {
                            _this.boardsList = $http.get('/api/boards').then(function (result) {
                                _this.boardsList = _.indexBy(result.data.data, 'id');
                                return _this.boardsList;
                            }, function (result) {
                                _this.boardsList = {};
                                return $q.reject(result);
                            });
                        }

                        return $q.when(_this.boardsList);
                    },
                    getBoard: function(id) {
                        var _this = this;

                        return this.getBoards().then(function(boards) {
                            if (_.isEmpty(boards[id])) {
                                _this.boardsList[id] = $http.get('/api/board', {params: {project_id: id}}).then(function(board){
                                    _this.boardsList[id] = board.data.data;
                                    return _this.boardsList[id];
                                });
                            }

                            return $q.when(_this.boardsList[id]);
                        });
                    },
                    addCardToBoard: function(card) {
                        var _this = this;
                        this.get(card.project_id).then(function (board) {
                            card.viewLabels = [];
                            for (var i = 0; i < card.labels.length; i++) {
                                var viewLabel = board.viewLabels[card.labels[i]];
                                if (viewLabel !== undefined) {
                                    card.viewLabels.push(viewLabel);
                                }
                            }
                            var stageLabel = _.intersection(board.labels, card.labels);
                            var found = false;
                            for (var stageName in board.stages) {
                                if (stageLabel == board.stages[stageName].label) {
                                    found = true;
                                    board.stages[stageName].cards.unshift(card);
                                }
                            }
                            if (!found) {
                                 board.stages[0].cards.unshift(card); 
                            }
                        });
                    },
                    removeCardFromBoard: function(card) {
                        var _this = this;
                        return _this.get(card.project_id).then(function (result) {
                            for (var stageName in result.stages) {
                                for (var cardIndex in result.stages[stageName].cards) {
                                    var cardToCheck = result.stages[stageName].cards[cardIndex];

                                    if (cardToCheck.id == card.id) {
                                        result.stages[stageName].cards.splice(cardIndex, 1);
                                    }
                                }
                            }
                        });
                    },
                    updateCardOnBoard: function(card) {
                        var _this = this;
                        return _this.get(card.project_id).then(function (board) {
                            _this.getCard(card.project_id, card.id).then(function(oldCard) {
                                for (var fieldName in card) {
                                    oldCard[fieldName] = card[fieldName];
                                }

                                oldCard.viewLabels = [];                                                                                                          
                                for (var i = 0; i < oldCard.labels.length; i++) {
                                    var viewLabel = board.viewLabels[oldCard.labels[i]];
                                    if (viewLabel !== undefined) { 
                                        oldCard.viewLabels.push(viewLabel);
                                    }
                                }

                                var stageLabel = _.intersection(board.labels, oldCard.labels);
                                var newStageLabel = _.intersection(board.labels, card.labels);
                                var found = false;

                                for (var stageName in board.stages) {
                                    var stage = board.stages[stageName];
                                    for (var cardIndex in stage.cards) {
                                        if (oldCard.id === stage.cards[cardIndex].id) {
                                            stage.cards.splice(cardIndex, 1);
                                        }
                                    }
                                }

                                for (var stageName in board.stages) {
                                    if (newStageLabel == board.stages[stageName].label) {
                                        found = true;
                                        board.stages[stageName].cards.unshift(oldCard);
                                        board.stages[stageName].cards = _.sortBy(board.stages[stageName].cards, function(c) {return c.iid*-1});
                                    }
                                }

                                if (!found) {
                                     board.stages[0].cards.unshift(card); 
                                }
                            });
                        });            
                    }
                };

                WebsocketService.on('card.create', function(data) {
                    return service.addCardToBoard(data);
                });

                WebsocketService.on('card.delete', function(data) {
                    return service.removeCardFromBoard(data);
                });

                WebsocketService.on('card.update', function(data) {
                    return service.updateCardOnBoard(data);
                });

                return service;
            }
        ]
    );
})(window.angular);

