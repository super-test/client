(function(angular) {
    'use strict';

    angular.module('gitlabKBApp.board').controller('TopBarController',
        [
            '$scope',
            '$state',
            '$stateParams',
            'BoardService',
            'store',
            function ($scope, $state, $stateParams, BoardService, store) {
                if ($stateParams.project_id !== undefined) {
                    BoardService.getBoard($stateParams.project_id).then(function (data) {
                        $scope.currentProject = data;
                    });
                }

                $scope.logout = function () {
                    store.remove('id_token');
                    $state.go('login');
                };

                $scope.state = $state;
            }
        ]
    );
})(window.angular);

