(function(angular){
    'use strict';

    angular.module('gitlabKBApp.user').factory('UserService', 
        [
            '$http', 
            '$q', 
            function ($http, $q) {
                return {
                    users: {},
                    list: function (boardId) {
                        return $q.when(this.users[boardId] == undefined ? $http.get('api/users', {params: {project_id: boardId}}).then(function (result) {
                            this.users[boardId] = result.data;
                            return this.users[boardId];
                        }.bind(this)) : this.users[boardId]);
                    }
                };
            }
        ]
    );
})(window.angular);
